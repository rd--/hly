all:
	echo "hly"

mk-cmd:
	echo "hly - NIL"

clean:
	rm -Rf dist dist-newstyle

push-all:
	r.gitlab-push.sh hly

push-tags:
	r.gitlab-push.sh hly --tags

indent:
	fourmolu -i Music

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Music
