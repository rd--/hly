-- | 'Pitch' names lifted to 'Music' values.
module Music.LilyPond.Light.Constant.Note where

import Music.LilyPond.Light.Model
import Music.Theory.Pitch {- hmt -}
import qualified Music.Theory.Pitch.Name as P

-- * Notes

pitch_to_music :: Pitch -> Music
pitch_to_music p = Note p Nothing []

c1, d1, e1, f1, g1, a1, b1 :: Music
c1 = pitch_to_music P.c1
d1 = pitch_to_music P.d1
e1 = pitch_to_music P.e1
f1 = pitch_to_music P.f1
g1 = pitch_to_music P.g1
a1 = pitch_to_music P.a1
b1 = pitch_to_music P.b1

ces1, des1, ees1, fes1, ges1, aes1, bes1 :: Music
ces1 = pitch_to_music P.ces1
des1 = pitch_to_music P.des1
ees1 = pitch_to_music P.ees1
fes1 = pitch_to_music P.fes1
ges1 = pitch_to_music P.ges1
aes1 = pitch_to_music P.aes1
bes1 = pitch_to_music P.bes1

cis1, dis1, eis1, fis1, gis1, ais1, bis1 :: Music
cis1 = pitch_to_music P.cis1
dis1 = pitch_to_music P.dis1
eis1 = pitch_to_music P.eis1
fis1 = pitch_to_music P.fis1
gis1 = pitch_to_music P.gis1
ais1 = pitch_to_music P.ais1
bis1 = pitch_to_music P.bis1

c2, d2, e2, f2, g2, a2, b2 :: Music
c2 = pitch_to_music P.c2
d2 = pitch_to_music P.d2
e2 = pitch_to_music P.e2
f2 = pitch_to_music P.f2
g2 = pitch_to_music P.g2
a2 = pitch_to_music P.a2
b2 = pitch_to_music P.b2

ces2, des2, ees2, fes2, ges2, aes2, bes2 :: Music
ces2 = pitch_to_music P.ces2
des2 = pitch_to_music P.des2
ees2 = pitch_to_music P.ees2
fes2 = pitch_to_music P.fes2
ges2 = pitch_to_music P.ges2
aes2 = pitch_to_music P.aes2
bes2 = pitch_to_music P.bes2

cis2, dis2, eis2, fis2, gis2, ais2, bis2 :: Music
cis2 = pitch_to_music P.cis2
dis2 = pitch_to_music P.dis2
eis2 = pitch_to_music P.eis2
fis2 = pitch_to_music P.fis2
gis2 = pitch_to_music P.gis2
ais2 = pitch_to_music P.ais2
bis2 = pitch_to_music P.bis2

cisis2, disis2, eisis2, fisis2, gisis2, aisis2, bisis2 :: Music
cisis2 = pitch_to_music P.cisis2
disis2 = pitch_to_music P.disis2
eisis2 = pitch_to_music P.eisis2
fisis2 = pitch_to_music P.fisis2
gisis2 = pitch_to_music P.gisis2
aisis2 = pitch_to_music P.aisis2
bisis2 = pitch_to_music P.bisis2

c3, d3, e3, f3, g3, a3, b3 :: Music
c3 = pitch_to_music P.c3
d3 = pitch_to_music P.d3
e3 = pitch_to_music P.e3
f3 = pitch_to_music P.f3
g3 = pitch_to_music P.g3
a3 = pitch_to_music P.a3
b3 = pitch_to_music P.b3

ces3, des3, ees3, fes3, ges3, aes3, bes3 :: Music
ces3 = pitch_to_music P.ces3
des3 = pitch_to_music P.des3
ees3 = pitch_to_music P.ees3
fes3 = pitch_to_music P.fes3
ges3 = pitch_to_music P.ges3
aes3 = pitch_to_music P.aes3
bes3 = pitch_to_music P.bes3

cis3, dis3, eis3, fis3, gis3, ais3, bis3 :: Music
cis3 = pitch_to_music P.cis3
dis3 = pitch_to_music P.dis3
eis3 = pitch_to_music P.eis3
fis3 = pitch_to_music P.fis3
gis3 = pitch_to_music P.gis3
ais3 = pitch_to_music P.ais3
bis3 = pitch_to_music P.bis3

cisis3, disis3, eisis3, fisis3, gisis3, aisis3, bisis3 :: Music
cisis3 = pitch_to_music P.cisis3
disis3 = pitch_to_music P.disis3
eisis3 = pitch_to_music P.eisis3
fisis3 = pitch_to_music P.fisis3
gisis3 = pitch_to_music P.gisis3
aisis3 = pitch_to_music P.aisis3
bisis3 = pitch_to_music P.bisis3

ceseh3, deseh3, eeseh3, feseh3, geseh3, aeseh3, beseh3 :: Music
ceseh3 = pitch_to_music P.ceseh3
deseh3 = pitch_to_music P.deseh3
eeseh3 = pitch_to_music P.eeseh3
feseh3 = pitch_to_music P.feseh3
geseh3 = pitch_to_music P.geseh3
aeseh3 = pitch_to_music P.aeseh3
beseh3 = pitch_to_music P.beseh3

ceh3, deh3, eeh3, feh3, geh3, aeh3, beh3 :: Music
ceh3 = pitch_to_music P.ceh3
deh3 = pitch_to_music P.deh3
eeh3 = pitch_to_music P.eeh3
feh3 = pitch_to_music P.feh3
geh3 = pitch_to_music P.geh3
aeh3 = pitch_to_music P.aeh3
beh3 = pitch_to_music P.beh3

cih3, dih3, eih3, fih3, gih3, aih3, bih3 :: Music
cih3 = pitch_to_music P.cih3
dih3 = pitch_to_music P.dih3
eih3 = pitch_to_music P.eih3
fih3 = pitch_to_music P.fih3
gih3 = pitch_to_music P.gih3
aih3 = pitch_to_music P.aih3
bih3 = pitch_to_music P.bih3

cisih3, disih3, eisih3, fisih3, gisih3, aisih3, bisih3 :: Music
cisih3 = pitch_to_music P.cisih3
disih3 = pitch_to_music P.disih3
eisih3 = pitch_to_music P.eisih3
fisih3 = pitch_to_music P.fisih3
gisih3 = pitch_to_music P.gisih3
aisih3 = pitch_to_music P.aisih3
bisih3 = pitch_to_music P.bisih3

c4, d4, e4, f4, g4, a4, b4 :: Music
c4 = pitch_to_music P.c4
d4 = pitch_to_music P.d4
e4 = pitch_to_music P.e4
f4 = pitch_to_music P.f4
g4 = pitch_to_music P.g4
a4 = pitch_to_music P.a4
b4 = pitch_to_music P.b4

ces4, des4, ees4, fes4, ges4, aes4, bes4 :: Music
ces4 = pitch_to_music P.ces4
des4 = pitch_to_music P.des4
ees4 = pitch_to_music P.ees4
fes4 = pitch_to_music P.fes4
ges4 = pitch_to_music P.ges4
aes4 = pitch_to_music P.aes4
bes4 = pitch_to_music P.bes4

cis4, dis4, eis4, fis4, gis4, ais4, bis4 :: Music
cis4 = pitch_to_music P.cis4
dis4 = pitch_to_music P.dis4
eis4 = pitch_to_music P.eis4
fis4 = pitch_to_music P.fis4
gis4 = pitch_to_music P.gis4
ais4 = pitch_to_music P.ais4
bis4 = pitch_to_music P.bis4

ceses4, deses4, eeses4, feses4, geses4, aeses4, beses4 :: Music
ceses4 = pitch_to_music P.ceses4
deses4 = pitch_to_music P.deses4
eeses4 = pitch_to_music P.eeses4
feses4 = pitch_to_music P.feses4
geses4 = pitch_to_music P.geses4
aeses4 = pitch_to_music P.aeses4
beses4 = pitch_to_music P.beses4

cisis4, disis4, eisis4, fisis4, gisis4, aisis4, bisis4 :: Music
cisis4 = pitch_to_music P.cisis4
disis4 = pitch_to_music P.disis4
eisis4 = pitch_to_music P.eisis4
fisis4 = pitch_to_music P.fisis4
gisis4 = pitch_to_music P.gisis4
aisis4 = pitch_to_music P.aisis4
bisis4 = pitch_to_music P.bisis4

ceseh4, deseh4, eeseh4, feseh4, geseh4, aeseh4, beseh4 :: Music
ceseh4 = pitch_to_music P.ceseh4
deseh4 = pitch_to_music P.deseh4
eeseh4 = pitch_to_music P.eeseh4
feseh4 = pitch_to_music P.feseh4
geseh4 = pitch_to_music P.geseh4
aeseh4 = pitch_to_music P.aeseh4
beseh4 = pitch_to_music P.beseh4

ceh4, deh4, eeh4, feh4, geh4, aeh4, beh4 :: Music
ceh4 = pitch_to_music P.ceh4
deh4 = pitch_to_music P.deh4
eeh4 = pitch_to_music P.eeh4
feh4 = pitch_to_music P.feh4
geh4 = pitch_to_music P.geh4
aeh4 = pitch_to_music P.aeh4
beh4 = pitch_to_music P.beh4

cih4, dih4, eih4, fih4, gih4, aih4, bih4 :: Music
cih4 = pitch_to_music P.cih4
dih4 = pitch_to_music P.dih4
eih4 = pitch_to_music P.eih4
fih4 = pitch_to_music P.fih4
gih4 = pitch_to_music P.gih4
aih4 = pitch_to_music P.aih4
bih4 = pitch_to_music P.bih4

cisih4, disih4, eisih4, fisih4, gisih4, aisih4, bisih4 :: Music
cisih4 = pitch_to_music P.cisih4
disih4 = pitch_to_music P.disih4
eisih4 = pitch_to_music P.eisih4
fisih4 = pitch_to_music P.fisih4
gisih4 = pitch_to_music P.gisih4
aisih4 = pitch_to_music P.aisih4
bisih4 = pitch_to_music P.bisih4

c5, d5, e5, f5, g5, a5, b5 :: Music
c5 = pitch_to_music P.c5
d5 = pitch_to_music P.d5
e5 = pitch_to_music P.e5
f5 = pitch_to_music P.f5
g5 = pitch_to_music P.g5
a5 = pitch_to_music P.a5
b5 = pitch_to_music P.b5

ces5, des5, ees5, fes5, ges5, aes5, bes5 :: Music
ces5 = pitch_to_music P.ces5
des5 = pitch_to_music P.des5
ees5 = pitch_to_music P.ees5
fes5 = pitch_to_music P.fes5
ges5 = pitch_to_music P.ges5
aes5 = pitch_to_music P.aes5
bes5 = pitch_to_music P.bes5

cis5, dis5, eis5, fis5, gis5, ais5, bis5 :: Music
cis5 = pitch_to_music P.cis5
dis5 = pitch_to_music P.dis5
eis5 = pitch_to_music P.eis5
fis5 = pitch_to_music P.fis5
gis5 = pitch_to_music P.gis5
ais5 = pitch_to_music P.ais5
bis5 = pitch_to_music P.bis5

ceses5, deses5, eeses5, feses5, geses5, aeses5, beses5 :: Music
ceses5 = pitch_to_music P.ceses5
deses5 = pitch_to_music P.deses5
eeses5 = pitch_to_music P.eeses5
feses5 = pitch_to_music P.feses5
geses5 = pitch_to_music P.geses5
aeses5 = pitch_to_music P.aeses5
beses5 = pitch_to_music P.beses5

cisis5, disis5, eisis5, fisis5, gisis5, aisis5, bisis5 :: Music
cisis5 = pitch_to_music P.cisis5
disis5 = pitch_to_music P.disis5
eisis5 = pitch_to_music P.eisis5
fisis5 = pitch_to_music P.fisis5
gisis5 = pitch_to_music P.gisis5
aisis5 = pitch_to_music P.aisis5
bisis5 = pitch_to_music P.bisis5

ceseh5, deseh5, eeseh5, feseh5, geseh5, aeseh5, beseh5 :: Music
ceseh5 = pitch_to_music P.ceseh5
deseh5 = pitch_to_music P.deseh5
eeseh5 = pitch_to_music P.eeseh5
feseh5 = pitch_to_music P.feseh5
geseh5 = pitch_to_music P.geseh5
aeseh5 = pitch_to_music P.aeseh5
beseh5 = pitch_to_music P.beseh5

ceh5, deh5, eeh5, feh5, geh5, aeh5, beh5 :: Music
ceh5 = pitch_to_music P.ceh5
deh5 = pitch_to_music P.deh5
eeh5 = pitch_to_music P.eeh5
feh5 = pitch_to_music P.feh5
geh5 = pitch_to_music P.geh5
aeh5 = pitch_to_music P.aeh5
beh5 = pitch_to_music P.beh5

cih5, dih5, eih5, fih5, gih5, aih5, bih5 :: Music
cih5 = pitch_to_music P.cih5
dih5 = pitch_to_music P.dih5
eih5 = pitch_to_music P.eih5
fih5 = pitch_to_music P.fih5
gih5 = pitch_to_music P.gih5
aih5 = pitch_to_music P.aih5
bih5 = pitch_to_music P.bih5

cisih5, disih5, eisih5, fisih5, gisih5, aisih5, bisih5 :: Music
cisih5 = pitch_to_music P.cisih5
disih5 = pitch_to_music P.disih5
eisih5 = pitch_to_music P.eisih5
fisih5 = pitch_to_music P.fisih5
gisih5 = pitch_to_music P.gisih5
aisih5 = pitch_to_music P.aisih5
bisih5 = pitch_to_music P.bisih5

c6, d6, e6, f6, g6, a6, b6 :: Music
c6 = pitch_to_music P.c6
d6 = pitch_to_music P.d6
e6 = pitch_to_music P.e6
f6 = pitch_to_music P.f6
g6 = pitch_to_music P.g6
a6 = pitch_to_music P.a6
b6 = pitch_to_music P.b6

ces6, des6, ees6, fes6, ges6, aes6, bes6 :: Music
ces6 = pitch_to_music P.ces6
des6 = pitch_to_music P.des6
ees6 = pitch_to_music P.ees6
fes6 = pitch_to_music P.fes6
ges6 = pitch_to_music P.ges6
aes6 = pitch_to_music P.aes6
bes6 = pitch_to_music P.bes6

cis6, dis6, eis6, fis6, gis6, ais6, bis6 :: Music
cis6 = pitch_to_music P.cis6
dis6 = pitch_to_music P.dis6
eis6 = pitch_to_music P.eis6
fis6 = pitch_to_music P.fis6
gis6 = pitch_to_music P.gis6
ais6 = pitch_to_music P.ais6
bis6 = pitch_to_music P.bis6

ceseh6, deseh6, eeseh6, feseh6, geseh6, aeseh6, beseh6 :: Music
ceseh6 = pitch_to_music P.ceseh6
deseh6 = pitch_to_music P.deseh6
eeseh6 = pitch_to_music P.eeseh6
feseh6 = pitch_to_music P.feseh6
geseh6 = pitch_to_music P.geseh6
aeseh6 = pitch_to_music P.aeseh6
beseh6 = pitch_to_music P.beseh6

ceh6, deh6, eeh6, feh6, geh6, aeh6, beh6 :: Music
ceh6 = pitch_to_music P.ceh6
deh6 = pitch_to_music P.deh6
eeh6 = pitch_to_music P.eeh6
feh6 = pitch_to_music P.feh6
geh6 = pitch_to_music P.geh6
aeh6 = pitch_to_music P.aeh6
beh6 = pitch_to_music P.beh6

cih6, dih6, eih6, fih6, gih6, aih6, bih6 :: Music
cih6 = pitch_to_music P.cih6
dih6 = pitch_to_music P.dih6
eih6 = pitch_to_music P.eih6
fih6 = pitch_to_music P.fih6
gih6 = pitch_to_music P.gih6
aih6 = pitch_to_music P.aih6
bih6 = pitch_to_music P.bih6

cisih6, disih6, eisih6, fisih6, gisih6, aisih6, bisih6 :: Music
cisih6 = pitch_to_music P.cisih6
disih6 = pitch_to_music P.disih6
eisih6 = pitch_to_music P.eisih6
fisih6 = pitch_to_music P.fisih6
gisih6 = pitch_to_music P.gisih6
aisih6 = pitch_to_music P.aisih6
bisih6 = pitch_to_music P.bisih6

c7, d7, e7, f7, g7, a7, b7 :: Music
c7 = pitch_to_music P.c7
d7 = pitch_to_music P.d7
e7 = pitch_to_music P.e7
f7 = pitch_to_music P.f7
g7 = pitch_to_music P.g7
a7 = pitch_to_music P.a7
b7 = pitch_to_music P.b7

ces7, des7, ees7, fes7, ges7, aes7, bes7 :: Music
ces7 = pitch_to_music P.ces7
des7 = pitch_to_music P.des7
ees7 = pitch_to_music P.ees7
fes7 = pitch_to_music P.fes7
ges7 = pitch_to_music P.ges7
aes7 = pitch_to_music P.aes7
bes7 = pitch_to_music P.bes7

cis7, dis7, eis7, fis7, gis7, ais7, bis7 :: Music
cis7 = pitch_to_music P.cis7
dis7 = pitch_to_music P.dis7
eis7 = pitch_to_music P.eis7
fis7 = pitch_to_music P.fis7
gis7 = pitch_to_music P.gis7
ais7 = pitch_to_music P.ais7
bis7 = pitch_to_music P.bis7
