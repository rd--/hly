-- | Dynamic constants.
module Music.LilyPond.Light.Constant.Dynamic where

import qualified Music.Theory.Dynamic_Mark as T {- hmt -}

import Music.LilyPond.Light.Model

dynamic_mark :: T.Dynamic_Mark -> Annotation
dynamic_mark dyn = Dynamic (Dynamic_Mark dyn)

pppp, ppp, pp, p, mp, mf, f, ff, fff, ffff, fp, sfz :: Annotation
pppp = dynamic_mark T.Pppp
ppp = dynamic_mark T.Ppp
pp = dynamic_mark T.Pp
p = dynamic_mark T.P
mp = dynamic_mark T.Mp
mf = dynamic_mark T.Mf
f = dynamic_mark T.F
ff = dynamic_mark T.Ff
fff = dynamic_mark T.Fff
ffff = dynamic_mark T.Ffff
fp = dynamic_mark T.Fp
sfz = dynamic_mark T.Sfz

cresc, decr, end_hairpin, end_cresc, end_decr, espressivo :: Annotation
cresc = Dynamic (Hairpin T.Crescendo)
decr = Dynamic (Hairpin T.Diminuendo)
end_hairpin = Dynamic (Hairpin T.End_Hairpin)
end_cresc = end_hairpin
end_decr = end_hairpin
espressivo = Dynamic Espressivo
