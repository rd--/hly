module Music.LilyPond.Light.Model where

import qualified Music.Theory.Clef as T {- hmt -}
{- hmt -}
{- hmt -}
import qualified Music.Theory.Duration as T {- hmt -}
import qualified Music.Theory.Dynamic_Mark as T {- hmt -}
import qualified Music.Theory.Key as T {- hmt -}
import qualified Music.Theory.Pitch as T
import qualified Music.Theory.Pitch.Note as T
import qualified Music.Theory.Time_Signature as T {- hmt -}

data Version = Version String
  deriving (Eq, Show)

data Units = Mm | Cm
  deriving (Eq, Show)

data Length = Length Double Units
  deriving (Eq, Show)

data Paper = Paper
  { binding_offset :: Length
  , bottom_margin :: Length
  , indent :: Length
  , left_or_inner_margin :: Length
  , paper_width :: Length
  , paper_height :: Length
  , ragged_right :: Bool
  , ragged_last :: Bool
  , ragged_bottom :: Bool
  , ragged_last_bottom :: Bool
  , right_or_outer_margin :: Length
  , top_margin :: Length
  , two_sided :: Bool
  , print_page_number :: Bool
  , min_systems_per_page :: Maybe Integer
  , max_systems_per_page :: Maybe Integer
  , systems_per_page :: Maybe Integer
  , systems_count :: Maybe Integer
  , page_count :: Maybe Integer
  , system_separator_markup :: Maybe String
  , system_spacing_basic_distance :: Maybe Integer
  , system_spacing_minimum_distance :: Maybe Integer
  }
  deriving (Eq, Show)

data Header = Header
  { dedication :: String
  , title :: String
  , subtitle :: String
  , subsubtitle :: String
  , instrument :: String
  , composer :: String
  , opus :: String
  , poet :: String
  , tagline :: String
  }
  deriving (Eq, Show)

data Articulation_Type
  = Accent
  | Arpeggio
  | ArpeggioDown
  | ArpeggioNeutral
  | ArpeggioUp
  | DownBow
  | Fermata
  | Flageolet
  | Glissando
  | Harmonic
  | LaissezVibrer
  | Marcato
  | Open
  | Portato
  | Staccato
  | Staccatissimo
  | StemTremolo Integer
  | Stopped
  | Tenuto
  | Trill
  | UpBow
  deriving (Eq, Show)

data Dynamic_Type
  = Dynamic_Mark T.Dynamic_Mark
  | Hairpin T.Hairpin
  | Espressivo
  deriving (Eq, Show)

data Phrasing_Type
  = Begin_Slur
  | End_Slur
  | Begin_PhrasingSlur
  | End_PhrasingSlur
  | Begin_Beam
  | End_Beam
  | SustainOn
  | SustainOff
  deriving (Eq, Show)

data Text_Type = Text_Symbol | Text_Plain | Text_Markup
  deriving (Eq, Show)

data Direction_Type = Direction_Up | Direction_Down | Direction_Default
  deriving (Eq, Show)

data Annotation
  = Articulation Articulation_Type
  | Dynamic Dynamic_Type
  | Phrasing Phrasing_Type
  | Begin_Tie
  | Direction Direction_Type
  | Text_Mark Text_Type String
  | Text Text_Type String
  | ReminderAccidental
  | CautionaryAccidental
  | Parentheses
  | CompositeAnnotation [Annotation]
  | Tweak String
  deriving (Eq, Show)

{- | Repeat barline types = ".|:", ":..:", ":|.|:", ":|.:", ":.|.:",
"[|:", ":|][|:" ":|]", ":|."
-}
data Bar_Type
  = NormalBarline
  | DoubleBarline
  | LeftRepeatBarline
  | RightRepeatBarline
  | FinalBarline
  | DottedBarline
  | DashedBarline
  | TickBarline
  | InvisibleBarline
  | UserBarline String
  deriving (Eq, Show)

data Command_Type
  = AutoBeamOff
  | Bar Bar_Type
  | BarlineCheck
  | BarNumberCheck Integer
  | Break -- Line break
  | Change String
  | DynamicDown
  | DynamicNeutral
  | DynamicUp
  | NoBreak
  | NoPageBreak
  | Octavation Integer
  | PageBreak
  | Partial T.Duration
  | Rehearsal_Mark (Maybe Int)
  | StemDown
  | StemNeutral
  | StemUp
  | TupletDown
  | TupletNeutral
  | TupletUp
  | User String
  | VoiceOne
  | VoiceTwo
  | VoiceThree
  | VoiceFour
  deriving (Eq, Show)

-- | (Denominator,Numerator)
type Tuplet_Type = (Integer, Integer)

data Tuplet_Mode
  = Normal_Tuplet
  | Scale_Durations
  deriving (Eq, Show)

-- | Type of rest.  Perhaps MmRest should be given here also.
data Rest_Type = Normal_Rest | Spacer_Rest deriving (Eq, Show)

data Music
  = Note
      { note_pitch :: T.Pitch
      , note_duration :: Maybe T.Duration
      , note_annotations :: [Annotation]
      }
  | Chord
      { chord_notes :: [Music]
      , chord_duration :: T.Duration
      , chord_annotations :: [Annotation]
      }
  | Tremolo (Either Music (Music, Music)) Integer
  | Rest Rest_Type T.Duration [Annotation]
  | MmRest Integer T.Time_Signature [Annotation]
  | Skip T.Duration [Annotation]
  | Repeat Integer Music
  | Tuplet Tuplet_Mode Tuplet_Type Music
  | Grace Music
  | AfterGrace Music Music
  | Join [Music]
  | Clef (T.Clef Int)
  | Time T.Time_Signature
  | Key T.Note (Maybe T.Alteration) T.Mode
  | Tempo (Maybe String) (Maybe (T.Duration, Rational))
  | Command Command_Type [Annotation]
  | Polyphony Music Music
  | Empty
  deriving (Eq, Show)

instance Semigroup Music where
  x <> y = Join [x, y]

instance Monoid Music where
  mempty = Empty
  mconcat = Join

type Staff_Name = (String, String)

type Staff_ID = String

data Staff_Type
  = Normal_Staff
  | Rhythmic_Staff
  deriving (Eq, Show)

-- data Part_Context = Lyrics String | Figured_Bass String

type Voice_Text = String
type Chord_Text = String

data Part
  = Part (Maybe Chord_Text) (Maybe Voice_Text) [Music]
  | MultipleParts [[Music]]
  deriving (Eq, Show)

data Staff_Set_Type
  = ChoirStaff
  | GrandStaff
  | PianoStaff
  | StaffGroup
  | StaffGroup_SquareBracket
  deriving (Eq, Show)

type Staff_Scalar = Int

data Staff_Settings = Staff_Settings Staff_Type Staff_ID Staff_Scalar
  deriving (Eq, Show)

data Staff
  = Staff Staff_Settings Staff_Name Part
  | Staff_Set Staff_Set_Type Staff_Name [Staff]
  deriving (Eq, Show)

data Score_Settings = Score_Settings
  { independent_time_signatures :: Bool
  , hide_time_signatures :: Bool
  , remove_empty_staves :: Bool
  -- ^ French score (hide empty staves)
  , remove_empty_staves_first_system :: Bool
  , omit_bar_numbers :: Bool
  , omit_bar_lines :: Bool
  , remove_stem_engraver :: Bool
  , default_bar_type :: String
  , staff_size :: Double
  -- ^ 20.0
  }
  deriving (Eq, Show)

data Score = Score Score_Settings [Staff]
  deriving (Eq, Show)

data Work = Work
  { work_version :: Version
  , work_paper :: Paper
  , work_header :: Header
  , work_score :: Score
  }
  deriving (Eq, Show)

data Fragment = Fragment
  { fragment_version :: Version
  , fragment_paper :: Paper
  , fragment_staff :: Staff
  }
  deriving (Eq, Show)

data Format = Pdf | Ps | Png
  deriving (Enum, Eq, Show)

-- * Default values

default_version :: Version
default_version = Version "2.24.1"

default_header :: Header
default_header =
  Header
    { dedication = ""
    , title = ""
    , subtitle = ""
    , subsubtitle = ""
    , instrument = ""
    , composer = ""
    , opus = ""
    , poet = ""
    , tagline = ""
    }

default_score_settings :: Score_Settings
default_score_settings =
  Score_Settings
    { independent_time_signatures = False
    , hide_time_signatures = False
    , remove_empty_staves = False
    , remove_empty_staves_first_system = False
    , omit_bar_numbers = False
    , omit_bar_lines = False
    , remove_stem_engraver = False
    , default_bar_type = "|"
    , staff_size = 20.0
    }

-- | (feta-font-size,staff-height-pt,staff-height-mm)
staff_size_list :: [(Int, Double, Double)]
staff_size_list =
  [ (11, 11.22, 3.9) -- pocket scores
  , (13, 12.60, 4.4)
  , (14, 14.14, 5.0)
  , (16, 15.87, 5.6)
  , (18, 17.82, 6.3)
  , (20, 20.00, 7.0) -- standard parts
  , (23, 22.45, 7.9)
  , (26, 25.20, 8.9)
  ]
