-- | Notation related constants.
module Music.LilyPond.Light.Constant where

import qualified Music.Theory.Clef as C {- hmt -}
import qualified Music.Theory.Duration as D {- hmt -}
import qualified Music.Theory.Tempo_Marking as T {- hmt -}

import Music.LilyPond.Light.Model {- hly -}

-- * Annotations

-- | Note annotations for pauses, let ring and so on.
arpeggio, fermata, flageolet, harmonic, laissezVibrer, glissando :: Annotation
arpeggio = Articulation Arpeggio
fermata = Articulation Fermata
flageolet = Articulation Flageolet
harmonic = Articulation Harmonic
laissezVibrer = Articulation LaissezVibrer
glissando = Articulation Glissando

marcato, staccato, staccatissimo, tenuto, accent :: Annotation
marcato = Articulation Marcato
staccato = Articulation Staccato
staccatissimo = Articulation Staccatissimo
tenuto = Articulation Tenuto
accent = Articulation Accent

stem_tremolo :: Integer -> Annotation
stem_tremolo = Articulation . StemTremolo

-- * Clefs

bass_clef, tenor_clef, alto_clef, treble_clef, percussion_clef :: Music
bass_clef = Clef (C.Clef C.Bass 0)
tenor_clef = Clef (C.Clef C.Tenor 0)
alto_clef = Clef (C.Clef C.Alto 0)
treble_clef = Clef (C.Clef C.Treble 0)
percussion_clef = Clef (C.Clef C.Percussion 0)

bass_8vb_clef, bass_15mb_clef, treble_8va_clef, treble_8vb_clef, treble_15ma_clef :: Music
bass_8vb_clef = Clef (C.Clef C.Bass (-1))
bass_15mb_clef = Clef (C.Clef C.Bass (-2))
treble_8va_clef = Clef (C.Clef C.Treble 1)
treble_8vb_clef = Clef (C.Clef C.Treble (-1))
treble_15ma_clef = Clef (C.Clef C.Treble 2)

-- * Commands

bar_line_check :: Music
bar_line_check = Command BarlineCheck []

normal_barline, double_barline, final_barline, tick_barline, dashed_barline, dotted_barline, invisible_barline :: Music
normal_barline = Command (Bar NormalBarline) []
double_barline = Command (Bar DoubleBarline) []
final_barline = Command (Bar FinalBarline) []
tick_barline = Command (Bar TickBarline) []
dashed_barline = Command (Bar DashedBarline) []
dotted_barline = Command (Bar DottedBarline) []
invisible_barline = Command (Bar InvisibleBarline) []

user_barline :: String -> Music
user_barline x = Command (Bar (UserBarline x)) []

system_break, no_system_break :: Music
system_break = Command Break []
no_system_break = Command NoBreak []

page_break, no_page_break :: Music
page_break = Command PageBreak []
no_page_break = Command NoPageBreak []

auto_beam_off :: Music
auto_beam_off = Command AutoBeamOff []

tuplet_down, tuplet_neutral, tuplet_up :: Music
tuplet_down = Command TupletDown []
tuplet_neutral = Command TupletNeutral []
tuplet_up = Command TupletUp []

voice_one, voice_two :: Music
voice_one = Command VoiceOne []
voice_two = Command VoiceTwo []

stem_down, stem_neutral, stem_up :: Music
stem_down = Command StemDown []
stem_neutral = Command StemNeutral []
stem_up = Command StemUp []

dynamic_down, dynamic_neutral, dynamic_up :: Music
dynamic_down = Command DynamicDown []
dynamic_neutral = Command DynamicNeutral []
dynamic_up = Command DynamicUp []

begin_8va, end_8va :: Music
begin_8va = Command (Octavation 1) []
end_8va = Command (Octavation 0) []

cadenzaOn :: Music
cadenzaOn = Command (User "\\cadenzaOn") []

-- * Annotations

ped, no_ped :: Annotation
ped = Phrasing SustainOn
no_ped = Phrasing SustainOff

tie :: Annotation
tie = Begin_Tie

-- | Beaming annotations.
begin_beam, end_beam :: Annotation
begin_beam = Phrasing Begin_Beam
end_beam = Phrasing End_Beam

-- | Slur annotations.
begin_slur, end_slur :: Annotation
begin_slur = Phrasing Begin_Slur
end_slur = Phrasing End_Slur

slur_down, slur_neutral, slur_up :: Music
slur_down = Command (User "\\slurDown") []
slur_neutral = Command (User "\\slurNeutral") []
slur_up = Command (User "\\slurUp") []

-- | Phrasing slur annotations.
begin_phrasing_slur, end_phrasing_slur :: Annotation
begin_phrasing_slur = Phrasing Begin_PhrasingSlur
end_phrasing_slur = Phrasing End_PhrasingSlur

-- * Accidentals

rAcc, cAcc :: Annotation
rAcc = ReminderAccidental
cAcc = CautionaryAccidental

set_accidental_style :: String -> Music
set_accidental_style ty =
  let x = "#(set-accidental-style '" ++ ty ++ ")"
  in Command (User x) []

set_accidental_style_dodecaphonic :: Music
set_accidental_style_dodecaphonic = set_accidental_style "dodecaphonic"

set_accidental_style_neo_modern :: Music
set_accidental_style_neo_modern = set_accidental_style "neo-modern"

set_accidental_style_modern :: Music
set_accidental_style_modern = set_accidental_style "modern"

-- * Noteheads

-- | Request particular note-heads (by style).
set_noteheads :: String -> Music
set_noteheads x =
  let u = "\\override NoteHead #'style = #'" ++ x
  in Command (User u) []

-- | Request cross note-heads.
cross_noteheads :: Music
cross_noteheads = set_noteheads "cross"

baroque_noteheads :: Music
baroque_noteheads = set_noteheads "baroque"

neomensural_noteheads, mensural_noteheads, petrucci_noteheads :: Music
neomensural_noteheads = set_noteheads "neomensural"
mensural_noteheads = set_noteheads "mensural"
petrucci_noteheads = set_noteheads "petrucci"

harmonic_noteheads, harmonic_mixed_noteheads, diamond_noteheads :: Music
harmonic_noteheads = set_noteheads "harmonic"
harmonic_mixed_noteheads = set_noteheads "harmonic-mixed"
diamond_noteheads = set_noteheads "diamond"

-- | Revert to standard note-heads (style).
revert_noteheads :: Music
revert_noteheads =
  Command (User "\\revert NoteHead #'style") []

{- | Set notehead glyph name.
This will make all noteheads the same, regardless of note duration.
This is useful for unmeasured music.

<https://lists.gnu.org/archive/html/lilypond-user/2011-03/msg00575.html>

> set_notehead_glyph "noteheads.s1"
-}
set_notehead_glyph :: String -> Music
set_notehead_glyph notehead_name =
  let txt =
        concat
          [ "\\override NoteHead #'stencil = #(lambda (grob)"
          , " (grob-interpret-markup grob"
          , "  (markup #:musicglyph \""
          , notehead_name
          , "\")))"
          ]
  in Command (User txt) []

-- * Aliases

tempo :: D.Duration -> Integer -> Music
tempo d k = Tempo Nothing (Just (d, fromIntegral k))

tempo_mark :: T.Tempo_Marking -> Music
tempo_mark mk = Tempo Nothing (Just mk)

tempo_text :: String -> D.Duration -> Integer -> Music
tempo_text str d k = Tempo (Just str) (Just (d, fromIntegral k))

tempo_text_only :: String -> Music
tempo_text_only str = Tempo (Just str) Nothing

after_grace :: Music -> Music -> Music
after_grace = AfterGrace

grace :: Music -> Music
grace = Grace

tremolo1 :: Music -> Integer -> Music
tremolo1 e = Tremolo (Left e)

tremolo2 :: (Music, Music) -> Integer -> Music
tremolo2 e = Tremolo (Right e)

slash_separator :: String
slash_separator = "\\slashSeparator"
