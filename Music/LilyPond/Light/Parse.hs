-- | <https://github.com/ejlilley/lilypond-parse>
module Music.LilyPond.Light.Parse where

{-

import Data.Maybe {- base -}
import Data.Ratio {- base -}

import qualified Text.Parsec as P {- parsec -}

import qualified Data.Music.Lilypond.Parse as L {- lilypond-parse -}

import qualified Music.Theory.Clef as T {- hmt -}
import qualified Music.Theory.Dynamic_Mark as T {- hmt -}
import qualified Music.Theory.Duration as T {- hmt -}
import qualified Music.Theory.Duration.RQ as T {- hmt -}
import qualified Music.Theory.Pitch as T {- hmt -}
import qualified Music.Theory.Pitch.Note as T {- hmt -}
import qualified Music.Theory.Time_Signature as T {- hmt -}

import qualified Music.LilyPond.Light as M {- hly -}

ratio_to_rational :: Integral i => Ratio i -> Rational
ratio_to_rational r = fromIntegral (numerator r) % fromIntegral (denominator r)

trs_note_duration :: L.LilyNoteDuration -> Maybe T.Duration
trs_note_duration r =
  if r == -1
  then Nothing
  else Just (T.rq_to_duration_err "trs_note_duration" (ratio_to_rational r * 4))

trs_time :: (Int, L.LilyNoteDuration) -> M.Music
trs_time (n,r) = M.Time (T.rq_to_ts (fromIntegral n * ratio_to_rational r * 4))

trs_note_name :: L.LilyNoteName -> (T.Note_T,Maybe T.Alteration_T)
trs_note_name nm =
  case P.runP T.p_note_alteration_ly () "" (show nm) of
    Left _ -> error "trs_note_name"
    Right r -> r

trs_octave :: L.LilyOctave -> T.Octave
trs_octave o =
  case o of
    L.LilyOctave n -> n + 3
    _ -> error "trs_octave"

trs_expressive :: L.LilyExpressive -> M.Annotation
trs_expressive e =
  case e of
    L.Artic a -> trs_articulation a
    L.Slur s -> trs_slur s
    L.Expr i -> trs_identifier i
    L.Super (L.Markup s) -> M.text_above_fmt s
    L.Sub (L.Markup s) -> M.text_below_fmt s
    _ -> error "trs_expressive"

trs_note :: L.LilyNote -> M.Music
trs_note n =
  case n of
    L.LilyNote nm oct dur ex ->
      let (nt,ac) = trs_note_name nm
          an = map trs_expressive ex
      in M.Note (T.Pitch nt (fromMaybe T.Natural ac) (trs_octave oct)) (trs_note_duration dur) an
    _ -> error "trs_note"

trs_articulation :: L.LilyArticulation -> M.Annotation
trs_articulation a =
  case a of
    L.ArticDash -> M.Articulation M.Marcato
    _ -> error "trs_articulation"

trs_slur :: L.LilySlur -> M.Annotation
trs_slur s =
  case s of
    L.SlurOn -> M.Phrasing M.Begin_Slur
    L.SlurOff -> M.Phrasing M.End_Slur
    _ -> error "trs_slur"

trs_identifier :: L.LilyIdentifier -> M.Annotation
trs_identifier (L.LilyIdentifier s) =
  case T.dynamic_mark_t_parse_ci s of
    Just dyn -> M.Dynamic (M.Dynamic_Mark dyn)
    _ -> error "trs_identifier"

trs_chord :: L.LilyChord -> M.Music
trs_chord (L.LilyChord nt dur) =
  M.Chord
  (map trs_note nt)
  (fromMaybe (error "trs_chord") (trs_note_duration dur))
  []

trs_clef :: L.LilyClef -> M.Music
trs_clef c =
  case c of
    L.Treble -> M.Clef (T.Clef T.Treble 0)
    L.Alto -> M.Clef (T.Clef T.Alto 0)
    L.Tenor -> M.Clef (T.Clef T.Tenor 0)
    L.Bass -> M.Clef (T.Clef T.Bass 0)
    _ -> error "trs_clef"

trs_expr :: L.LilyExpr -> M.Music
trs_expr e =
  case e of
    L.Note n -> trs_note n
    L.Chord c -> trs_chord c
    L.Time t -> trs_time t
    L.Clef c -> trs_clef c
    _ -> error "trs_expr"

ly_parse_expr :: String -> M.Music
ly_parse_expr s =
  case P.runP L.parseExpr () "." s of
    Right e -> trs_expr e
    _ -> error "ly_parse_expr"

ly_parse_seq :: String -> [M.Music]
ly_parse_seq s =
  case P.runP L.parseSeq () "." s of
    Right (L.LilySequential r) -> map trs_expr r
    _ -> error "ly_parse_seq"

{-

ly = ["\\time 3/4","\\clef treble","a4\\mf","b,--\\pp","c'(","d'')","<d e>2","a4^\"+23\""]
mus = map ly_parse_expr ly
map M.ly_music_elem mus
mus !! 3
mus = ly_parse_seq (unwords (M.with_braces ly))
mus

-}
-}
