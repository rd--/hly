module Music.LilyPond.Light.Internal where

import Text.Printf {- base -}

import Music.LilyPond.Light.Model

mk_cmd :: String -> Music
mk_cmd x = Command (User x) []

scm_bool :: Bool -> String
scm_bool c = if c then "##t" else "##f"

{- | Scheme pair

>>> scm_pair (3,4)
"#'(3 . 4)"
-}
scm_pair :: (Show x, Show y) => (x, y) -> String
scm_pair (x, y) = "#'(" ++ show x ++ " . " ++ show y ++ ")"

scm_null :: String
scm_null = "#'()"

scm_int :: Int -> String
scm_int = ('#' :) . show

scm_double :: Double -> String
scm_double = ('#' :) . show

scm_symbol :: String -> String
scm_symbol = (++) "'#"

-- * Override and set, c.f. <https://lilypond.org/doc/v2.22/Documentation/notation/set-versus-override>

override_property :: String -> String -> String -> Music
override_property z x y = mk_cmd (printf "\\override %s #'%s = %s" x z y)

override_field :: String -> String -> String -> Music
override_field z x y = mk_cmd (printf "\\override %s.%s = %s" x z y)

override_stencil :: String -> String -> Music
override_stencil = override_property "stencil"

set_field :: String -> String -> String -> Music
set_field z x y = mk_cmd (printf "\\set %s.%s = %s" x z y)
