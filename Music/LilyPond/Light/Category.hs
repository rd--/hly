module Music.LilyPond.Light.Category where

import Music.LilyPond.Light.Model

-- | 'Music' element category enumeration.
data Music_C
  = Note_C
  | Chord_C
  | Tremolo_C
  | Rest_C
  | MmRest_C
  | Skip_C
  | Repeat_C
  | Tuplet_C
  | Grace_C
  | AfterGrace_C
  | Join_C
  | Clef_C
  | Time_C
  | Key_C
  | Tempo_C
  | Command_C
  | Polyphony_C
  | Empty_C
  deriving (Eq, Enum, Bounded)

-- | Categorise 'Music' element.
music_c :: Music -> Music_C
music_c m =
  case m of
    Note {} -> Note_C
    Chord {} -> Chord_C
    Tremolo _ _ -> Tremolo_C
    Rest _ _ _ -> Rest_C
    MmRest {} -> MmRest_C
    Skip _ _ -> Skip_C
    Repeat _ _ -> Repeat_C
    Tuplet {} -> Tuplet_C
    Grace _ -> Grace_C
    AfterGrace _ _ -> AfterGrace_C
    Join _ -> Join_C
    Clef _ -> Clef_C
    Time _ -> Time_C
    Key {} -> Key_C
    Tempo _ _ -> Tempo_C
    Command _ _ -> Command_C
    Polyphony _ _ -> Polyphony_C
    Empty -> Empty_C

-- * Music category predicates

is_music_c :: Music_C -> Music -> Bool
is_music_c c = (==) c . music_c

is_note :: Music -> Bool
is_note = is_music_c Note_C

is_chord :: Music -> Bool
is_chord = is_music_c Chord_C

is_rest :: Music -> Bool
is_rest = is_music_c Rest_C

is_skip :: Music -> Bool
is_skip = is_music_c Skip_C

is_mm_rest :: Music -> Bool
is_mm_rest = is_music_c MmRest_C

is_grace :: Music -> Bool
is_grace = is_music_c Grace_C

is_after_grace :: Music -> Bool
is_after_grace = is_music_c AfterGrace_C

is_clef :: Music -> Bool
is_clef = is_music_c Clef_C

is_time :: Music -> Bool
is_time = is_music_c Time_C

is_tempo :: Music -> Bool
is_tempo = is_music_c Tempo_C

is_command :: Music -> Bool
is_command = is_music_c Command_C

is_tuplet :: Music -> Bool
is_tuplet = is_music_c Tuplet_C
