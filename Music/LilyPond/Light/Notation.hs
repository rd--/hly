module Music.LilyPond.Light.Notation where

import Data.List {- base -}
import Data.Maybe {- base -}
import Data.Ratio {- base -}
import Text.Printf {- base -}

import qualified Data.List.Split as Split {- split -}

import qualified Music.Theory.Duration as T {- hmt -}
import qualified Music.Theory.Duration.Annotation as T {- hmt -}
import qualified Music.Theory.Duration.Rq as Rq {- hmt -}
import qualified Music.Theory.Duration.Sequence.Notate as Notate {- hmt -}
import qualified Music.Theory.Key as Key {- hmt -}
import qualified Music.Theory.Pitch as Pitch {- hmt -}
import qualified Music.Theory.Time_Signature as Time_Signature {- hmt -}

import Music.LilyPond.Light.Category
import Music.LilyPond.Light.Constant
import Music.LilyPond.Light.Internal
import Music.LilyPond.Light.Measure
import Music.LilyPond.Light.Model
import Music.LilyPond.Light.Output.LilyPond
import Music.LilyPond.Light.Paper

-- | These are required to avoid issues in lilypond (see manual)
is_grace_skip :: Music -> Bool
is_grace_skip m =
  case m of
    Grace (Skip _ _) -> True
    _ -> False

is_barlinecheck :: Music -> Bool
is_barlinecheck m =
  case m of
    Command BarlineCheck _ -> True
    _ -> False

is_tied :: Music -> Bool
is_tied m =
  case m of
    Note _ _ xs -> Begin_Tie `elem` xs
    Chord _ _ xs -> Begin_Tie `elem` xs
    _ -> False

-- * Duration

-- | If 'Music' is a 'Note', 'Chord' or 'Rest' give duration, else 'Nothing'.
music_immediate_duration :: Music -> Maybe T.Duration
music_immediate_duration m =
  case m of
    Note _ d _ -> d
    Chord _ d _ -> Just d
    Rest _ d _ -> Just d
    _ -> Nothing

-- * Pitch

-- | Remove any reminder or cautionary accidentals at note or chord.
clr_acc :: Music -> Music
clr_acc m =
  let rl = [rAcc, cAcc]
  in case m of
      Note x d a -> Note x d (a \\ rl)
      Chord xs d a -> Chord (map clr_acc xs) d a
      _ -> error ("clr_acc at non-note/chord: " ++ ly_music_elem m)

octpc_to_note :: (Pitch.Octave, Pitch.PitchClass) -> Music
octpc_to_note x = Note (Pitch.octpc_to_pitch Pitch.pc_spell_ks x) Nothing []

pitch_to_note :: Pitch.Pitch -> Music
pitch_to_note p = Note p Nothing []

-- * Rests

-- | Construct normal rest.
rest :: T.Duration -> Music
rest x = Rest Normal_Rest x []

-- | Construct spacer rest.
spacer_rest :: T.Duration -> Music
spacer_rest x = Rest Spacer_Rest x []

-- | Multi-measure variant of 'rest'.
mm_rest :: Time_Signature.Time_Signature -> Music
mm_rest x = MmRest 1 x []

-- | Non-printing variant of 'rest'.
skip :: T.Duration -> Music
skip x = Skip x []

-- | Create an empty measure for the specified time signature.
empty_measure :: Time_Signature.Time_Signature -> Music
empty_measure ts = mconcat [MmRest 1 ts []] -- bar_line_check

-- | Like 'empty_measure', but with an invisible rest.
null_measure :: Time_Signature.Time_Signature -> Music
null_measure (n, d) =
  let x = T.Duration d 0 1
  in mconcat (map (\i -> Skip i []) (genericReplicate n x)) -- [bar_line_check]

-- | Like 'empty_measure' but write time signature.
measure_rest :: Time_Signature.Time_Signature -> Music
measure_rest ts = mconcat [time_signature ts, empty_measure ts]

-- | Like 'measure_rest' but write time signature.
measure_null :: Time_Signature.Time_Signature -> Music
measure_null ts = mconcat [time_signature ts, null_measure ts]

-- * Tuplets

-- | Apply a 'Duration' function to a 'Music' node, if it has a duration.
edit_dur :: (T.Duration -> T.Duration) -> Music -> Music
edit_dur fn x =
  case x of
    Note _ Nothing _ -> x
    Note n (Just d) a -> Note n (Just (fn d)) a
    Chord n d a -> Chord n (fn d) a
    Rest ty d a -> Rest ty (fn d) a
    Skip d a -> Skip (fn d) a
    _ -> x

-- | Temporal scaling of music (tuplets).
tuplet :: Tuplet_Type -> [Music] -> Music
tuplet (d, n) =
  let fn x = x {T.multiplier = n % d}
  in Tuplet Normal_Tuplet (n, d) . mconcat . map (edit_dur fn)

-- | Tuplet variants that set location, and then restore to neutral.
tuplet_above, tuplet_below :: Tuplet_Type -> [Music] -> Music
tuplet_above n xs = mconcat [tuplet_up, tuplet n xs, tuplet_neutral]
tuplet_below n xs = mconcat [tuplet_down, tuplet n xs, tuplet_neutral]

{- | Like tuplet but does not annotate music, see also
  'ts_set_fraction'.
-}
scale_durations :: Tuplet_Type -> [Music] -> Music
scale_durations (n, d) =
  let fn x = x {T.multiplier = d % n}
  in Tuplet Scale_Durations (n, d) . mconcat . map (edit_dur fn)

-- * Time signatures

-- | Construct time signature.
time_signature :: Time_Signature.Time_Signature -> Music
time_signature = Time

-- | Allow proper auto-indenting of multiple measures with the same time signature.
with_time_signature :: Time_Signature.Time_Signature -> [Music] -> Music
with_time_signature ts xs = mconcat (time_signature ts : xs)

{-
-- | Make a duration to fill a whole measure.
ts_dur :: Time_Signature -> Duration
ts_dur (n,d) = Duration d 0 (fromIntegral n)
-}

-- | Command to request that @4\/4@ and @2\/2@ etc. are typeset as fractions.
ts_use_fractions :: Music
ts_use_fractions = override_property "style" "Staff.TimeSignature" scm_null

-- | Set the printed time-signature fraction.
ts_set_fraction :: Integer -> Integer -> Music
ts_set_fraction n d = mk_cmd ("\\set Staff.timeSignatureFraction = " ++ scm_pair (n, d))

numeric_time_signature :: Music
numeric_time_signature = mk_cmd "\\numericTimeSignature"

-- | Hide time signatures if 'False'.
ts_stencil :: Bool -> Music
ts_stencil x = override_stencil "Staff.TimeSignature" (scm_bool x)

-- | Hide barline signatures if 'False'.
barline_stencil :: Bool -> Music
barline_stencil x = override_stencil "Score.BarLine" (scm_bool x)

-- | Hide metronome mark if 'False'.
mm_stencil :: Bool -> Music
mm_stencil x = override_stencil "Score.MetronomeMark" (scm_bool x)

ts_transparent :: Bool -> Music
ts_transparent x = override_property "transparent" "Staff.TimeSignature" (scm_bool x)

ts_all_invisible :: Music
ts_all_invisible = override_property "break-visibility" "Staff.TimeSignature" "#all-invisible"

rehearsal_mark_padding :: Double -> Music
rehearsal_mark_padding = override_field "padding" "Score.RehearsalMark" . scm_double

-- | -1 = left, 0 = center, 1 = right.
rehearsal_mark_alignment :: Int -> Music
rehearsal_mark_alignment = override_field "self-alignment-X" "Score.RehearsalMark" . scm_int

ts_parentheses :: Music
ts_parentheses =
  override_stencil
    "Staff.TimeSignature"
    "#(lambda (grob) (bracketify-stencil (ly:time-signature::print grob) Y 0.1 0.2 0.1))"

{- | https://lilypond.org/doc/v2.23/Documentation/notation/proportional-notation

>>> set_proportional_notation_duration 8
Command (User "\\set Score.proportionalNotationDuration = #(ly:make-moment 1/8)") []
-}
set_proportional_notation_duration :: Int -> Music
set_proportional_notation_duration n = set_field "proportionalNotationDuration" "Score" (printf "#(ly:make-moment 1/%d)" n)

-- | https://lilypond.org/doc/v2.23/Documentation/notation/proportional-notation
set_uniform_stretching :: Bool -> Music
set_uniform_stretching = override_field "uniform-stretching" "Score.SpacingSpanner" . scm_bool

-- | https://lilypond.org/doc/v2.23/Documentation/notation/proportional-notation
set_strict_note_spacing :: Bool -> Music
set_strict_note_spacing = override_field "strict-note-spacing" "Score.SpacingSpanner" . scm_bool

-- * Key signatures

-- | Construct key signature.
key :: Key.Key -> Music
key (n, a, md) = Key n (Just a) md

-- * Repetition

-- | Construct standard (two times) repeat.
std_repeat :: Integer -> [Music] -> Music
std_repeat n = Repeat n . mconcat

-- * Octave

-- | Shift the octave of a note element, else identity.
note_edit_octave :: (Pitch.Octave -> Pitch.Octave) -> Music -> Music
note_edit_octave fn m =
  case m of
    Note (Pitch.Pitch n a o) d xs -> Note (Pitch.Pitch n a (fn o)) d xs
    _ -> m

-- | Shift the octave of a note element, else identity.
note_shift_octave :: Pitch.Octave -> Music -> Music
note_shift_octave i = note_edit_octave (+ i)

-- * Duration

{- | Tie right

>>> tie_r_ann [T.Tie_Right]
[Begin_Tie]
-}
tie_r_ann :: [T.D_Annotation] -> [Annotation]
tie_r_ann a = if any (== T.Tie_Right) a then [Begin_Tie] else []

{- | If there is a 'T.Tie_Left', then clear the appropriate annotations.
(Actually just all...)
-}
clear_l_ann :: [T.D_Annotation] -> [Annotation] -> [Annotation]
clear_l_ann d_a m_a = if any (== T.Tie_Left) d_a then [] else m_a

-- | Rest of  'Duration_A'.
da_rest :: T.Duration_A -> Music
da_rest (d, _) = Rest Normal_Rest d []

-- | Add 'Duration_A' to 'Pitch' to make a @Note@ 'Music' element.
(##@) :: Pitch.Pitch -> T.Duration_A -> Music
x ##@ (d, a) = Note x (Just d) (tie_r_ann a)

-- | Add 'Duration' to 'Pitch' to make a @Note@ 'Music' element.
(##) :: Pitch.Pitch -> T.Duration -> Music
x ## d = x ##@ (d, [])

{- | Add 'Duration_A' to either a @Note@, @Chord@ or @Rest@ 'Music' element.
     Also adds to the middle element of a three element Join node, though this is very special case!
     It perhaps should add to a Join iff there is one applicable element in it.
     Todo.
-}
(#@) :: Music -> T.Duration_A -> Music
x #@ (d, a) =
  case x of
    Note n _ a' -> Note n (Just d) (tie_r_ann a ++ clear_l_ann a a')
    Chord n _ a' -> Chord n d (tie_r_ann a ++ clear_l_ann a a')
    Rest ty _ a' -> Rest ty d (clear_l_ann a a')
    Join [p, q, r] -> Join [p, q #@ (d, a), r]
    _ -> error ("#@: " ++ show x)

-- | Add 'Duration' to either a @Note@ or @Chord@ or @Rest@ 'Music' element.
(#) :: Music -> T.Duration -> Music
x # d =
  case x of
    Rest ty _ a -> Rest ty d a
    _ -> x #@ (d, [])

-- * Chords

-- | Construct chord from 'Pitch' elements.
chd_p_ann :: [Pitch.Pitch] -> [[Annotation]] -> T.Duration -> Music
chd_p_ann xs an d =
  let f x a = Note x Nothing a
  in case xs of
      [] -> error "chd_p_ann: null elements"
      _ -> Chord (zipWith f xs an) d []

-- | Construct chord from 'Pitch' elements.
chd_p :: [Pitch.Pitch] -> T.Duration -> Music
chd_p xs = chd_p_ann xs (repeat [])

-- | Construct chord from 'Music' elements.
chd :: [Music] -> T.Duration -> Music
chd xs d =
  case xs of
    [] -> error "chd: null elements"
    _ ->
      let fn x =
            let err msg = error (msg ++ ": " ++ show x)
            in case x of
                Note _ (Just _) _ -> err "chd: note has duration"
                Note _ Nothing _ -> x
                _ -> err "chd: non note element"
      in Chord (map fn xs) d []

-- * Commands

-- | Construct bar number check command.
bar_number_check :: Integer -> Music
bar_number_check n = Command (BarNumberCheck n) []

-- | Switch bar numbering visibility.
bar_numbering :: Bool -> Music
bar_numbering x =
  let r = if x then "##(#t #t #t)" else "##(#f #f #f)"
  in override_property "break-visibility" "Score.BarNumber" r

-- | Change staff (for cross staff notation).
change :: String -> Music
change x = Command (Change x) []

-- | Indicate initial partial measure.
partial :: T.Duration -> Music
partial d = Command (Partial d) []

-- | Set or unset the @circled-tip@ hairpin attribute.
hairpin_circled_tip :: Bool -> Music
hairpin_circled_tip x =
  let c =
        if x
          then "\\override Hairpin #'circled-tip = ##t"
          else "\\revert Hairpin #'circled-tip"
  in mk_cmd c

-- | Set or unset the @to-barline@ hairpin attribute.
hairpin_to_barline :: Bool -> Music
hairpin_to_barline x =
  let c =
        if x
          then "\\revert Hairpin #'to-barline"
          else "\\override Hairpin #'to-barline = ##f"
  in mk_cmd c

-- | Set or unset the @minimum-length@ hairpin attribute.
hairpin_minimum_length :: Maybe Int -> Music
hairpin_minimum_length x =
  let c = case x of
        Nothing -> "\\revert Hairpin.minimum-length"
        Just n -> "\\override Hairpin.minimum-length = #" ++ show n
  in mk_cmd c

-- * Staff and Parts

set_8va_notation :: Music
set_8va_notation = mk_cmd "\\set Staff.ottavation = #\"8\""

name_to_id :: Staff_Name -> Staff_ID
name_to_id (x, _) =
  case x of
    "" -> "no_id"
    _ -> "id_" ++ x

staff_line_count :: Int -> Music
staff_line_count n = override_field "line-count" "Staff.StaffSymbol" (show n)

mk_staff :: Staff_Type -> Staff_Name -> [Music] -> Staff
mk_staff ty nm =
  let st = Staff_Settings ty (name_to_id nm) 0
  in Staff st nm . Part Nothing Nothing

-- | Construct staff.
staff :: Staff_Name -> [Music] -> Staff
staff = mk_staff Normal_Staff

-- | Construct rhythmic staff.
rhythmic_staff :: Staff_Name -> [Music] -> Staff
rhythmic_staff = mk_staff Rhythmic_Staff

-- | Construct staff with text underlay.
text_staff :: Staff_Name -> String -> [Music] -> Staff
text_staff nm txt =
  let st = Staff_Settings Normal_Staff (name_to_id nm) 0
  in Staff st nm . Part Nothing (Just txt)

{- | Construct piano staff.  For two staff piano music the staffs have
  identifiers rh and lh.
-}
piano_staff :: Staff_Name -> [[Music]] -> Staff
piano_staff nm xs =
  case xs of
    [rh, lh] ->
      let st x = Staff_Settings Normal_Staff x 0
      in Staff_Set
          PianoStaff
          nm
          [ Staff (st "rh") ("", "") (Part Nothing Nothing rh)
          , Staff (st "lh") ("", "") (Part Nothing Nothing lh)
          ]
    _ -> Staff_Set PianoStaff nm (map (mk_staff Normal_Staff ("", "")) xs)

grand_staff :: Staff_Name -> [[Music]] -> Staff
grand_staff nm = Staff_Set GrandStaff nm . map (staff ("", ""))

staff_group :: Staff_Name -> [[Music]] -> Staff
staff_group nm = Staff_Set StaffGroup nm . map (staff ("", ""))

rhythmic_grand_staff :: Staff_Name -> [[Music]] -> Staff
rhythmic_grand_staff nm = Staff_Set GrandStaff nm . map (rhythmic_staff ("", ""))

-- | Variant with names for each staff.
staff_set :: (Staff_Set_Type, Staff_Type) -> Staff_Name -> [Staff_Name] -> [[Music]] -> Staff
staff_set (set_ty, stf_ty) nm xs ys = Staff_Set set_ty nm (zipWith (mk_staff stf_ty) xs ys)

mk_grand_staff :: Staff_Name -> [Staff_Name] -> [[Music]] -> Staff
mk_grand_staff = staff_set (GrandStaff, Normal_Staff)

mk_staff_group :: Staff_Name -> [Staff_Name] -> [[Music]] -> Staff
mk_staff_group = staff_set (StaffGroup, Normal_Staff)

two_part_staff :: Staff_Name -> ([Music], [Music]) -> Staff
two_part_staff nm (p0, p1) =
  let st = Staff_Settings Normal_Staff (name_to_id nm) 0
  in Staff
      st
      nm
      ( MultipleParts
          [ voice_one : p0
          , voice_two : p1
          ]
      )

instr_name :: Staff_Name -> Staff -> Staff
instr_name nm pt =
  case pt of
    Staff st _ x -> Staff st nm x
    Staff_Set ty _ xs -> Staff_Set ty nm xs

resize_staff :: Int -> Staff -> Staff
resize_staff n st =
  case st of
    Staff (Staff_Settings ty i sc) nm pt ->
      Staff (Staff_Settings ty i (sc + n)) nm pt
    Staff_Set ty nm xs ->
      Staff_Set ty nm (map (resize_staff n) xs)

score :: [Staff] -> Score
score = Score default_score_settings

{- | Interior polyphony.  For two part music on one staff see
  'two_part_staff'.
-}
polyphony :: Music -> Music -> Music
polyphony = Polyphony

polyphony' :: [Music] -> [Music] -> Music
polyphony' x y = polyphony (mconcat x) (mconcat y)

-- * Rests

join_direct_rests_maybe :: Music -> Music -> Maybe Music
join_direct_rests_maybe p q =
  case (p, q) of
    (Rest _t1 d1 a1, Rest t2 d2 a2) ->
      case T.sum_dur d1 d2 of
        Just d3 -> Just (Rest t2 d3 (a1 ++ a2))
        Nothing -> Nothing
    _ -> Nothing

{- | Joins directly adjacent rest elements.
     Rest type is adopted from the right when joining.
     Tuplets of only rests should be replaced by a rest of the appropriate duration.
     This function should be applied firstly to each sub-division of a measure separately,
     and then to the measure as a whole.
-}
join_rests :: [Music] -> [Music]
join_rests =
  let fn recur xs =
        case xs of
          [] -> []
          Rest t1 d1 a1 : Rest t2 d2 a2 : ys ->
            case T.sum_dur d1 d2 of
              Nothing ->
                let zs = Rest t1 d1 a1 : join_rests (Rest t2 d2 a2 : ys)
                in if recur then fn False zs else zs
              Just d3 -> join_rests (Rest t2 d3 (a1 ++ a2) : ys)
          Tuplet md ty (Join mus) : ys -> Tuplet md ty (Join (join_rests mus)) : join_rests ys
          y : ys -> y : join_rests ys
  in fn True

-- | join_rests within each measure.
mm_join_rests :: [Measure] -> [Measure]
mm_join_rests = map (\(Measure a m) -> Measure a (join_rests m))

-- * 'Duration_A' functions

-- | Transform ascribed 'Duration_A' value to 'Music'.
type DA_F x = (T.Duration_A, x) -> Music

{- | Given 'DA_F' transform, transform set of ascribed 'Duration_A' values to 'Music'.

> import Music.Theory.Pitch.Name as T

> let t = True
> let f = False
> let Right d = Notate.m_notate True [[(2/3,f),(1/3,t)],[(1,t)],[(1,f)]]
> let jn (i,j) = j ##@ i
> let n = Notate.ascribe d [c4,d4]
> let r = "\\times 2/3 {  c' 4  d' 8 ~ }  d' 4 ~  d' 4"
> ly_music_elem (Join (da_to_music jn n)) == r
-}
da_to_music :: DA_F t -> [(T.Duration_A, t)] -> [Music]
da_to_music fn x =
  let g = T.da_group_tuplets_nn (map fst x)
      g' = T.nn_reshape (,) g (map snd x)
      tr el = case el of
        Left i -> fn i
        Right y ->
          let (y0, _) = head y
              (n, d, _) = fromJust (T.da_begin_tuplet y0)
          in Tuplet Normal_Tuplet (d, n) (Join (map fn y))
  in map tr g'

-- | Variant of 'da_to_music' that operates on sets of measures.
da_to_measures :: DA_F x -> Maybe [Time_Signature.Time_Signature] -> [[(T.Duration_A, x)]] -> [Measure]
da_to_measures fn m_t x =
  let m = map (da_to_music fn) x
      jn i = Measure [i]
  in case m_t of
      Just t -> zipWith jn (map Time t) m
      Nothing -> map (Measure []) m

{- | 'da_to_measures' of 'notate_mm_ascribe'.

> import Music.Theory.Pitch.Name as T
> import Music.LilyPond.Light.Output.LilyPond as L

> let {jn (i,j) = j ##@ i
>     ;[Measure _ m] = rq_to_measures 4 jn [] [(3,4)] Nothing [2/3,1/3 + 2] [c4,d4]
>     ;r = "\\times 2/3 { c' 4 d' 8 ~ } d' 2"}
> in L.ly_music_elem (Join m) == r
-}
rq_to_measures :: (Show x) => Int -> DA_F x -> [Notate.Simplify_T] -> [Time_Signature.Time_Signature] -> Maybe [[Rq.Rq]] -> [Rq.Rq] -> [x] -> [Measure]
rq_to_measures limit fn r ts rqp rq x =
  let da = Notate.notate_mm_ascribe_err limit r ts rqp rq x
  in da_to_measures fn (Just ts) da

-- * Fragment

{- | Make a fragment (possibly multiple staffs) from 'Music' elements.
Width and height are in millimeters.
-}
mk_fragment :: (Double, Double) -> [[Music]] -> Fragment
mk_fragment (w, h) m =
  let pr = mk_fragment_paper w h
  in Fragment default_version pr (grand_staff ("", "") m)

-- | 'Measure' variant of 'mk_fragment'.
mk_fragment_mm :: (Double, Double) -> [[Measure]] -> Fragment
mk_fragment_mm d = mk_fragment d . map mm_elements

-- * Stem

{- | Transparent stem

>>> stem_transparent True
Command (User "\\override Stem #'transparent = ##t") []
-}
stem_transparent :: Bool -> Music
stem_transparent x = override_property "transparent" "Stem" (scm_bool x)

-- * Text

-- | Make text annotations respace music to avoid vertical displacement.
text_length_on, text_length_off :: Music
text_length_on = mk_cmd "\\textLengthOn"
text_length_off = mk_cmd "\\textLengthOff"

text_outside_staff_priority :: Maybe Double -> Music
text_outside_staff_priority x =
  let pr = case x of
        Nothing -> scm_bool False
        Just n -> scm_double n
  in override_property "outside-staff-priority" "TextScript" pr

text_extra_spacing_width :: (Double, Double) -> Music
text_extra_spacing_width w = override_property "extra-spacing-width" "TextScript" (scm_pair w)

-- * Measure operations

{- | Delete (remove) redundant (repeated, duplicated) time signatures.

>>> let mm = [Measure [Time (3,4)] [],Measure [Time (3,4)] []]
>>> mm_delete_redundant_ts mm
[Measure [Time (3,4)] [],Measure [] []]
-}
mm_delete_redundant_ts :: [Measure] -> [Measure]
mm_delete_redundant_ts =
  let f st m =
        let Measure a n = m
            ts = find is_time a
        in case (st, ts) of
            (Just p, Just q) ->
              if p == q
                then (st, Measure (delete q a) n)
                else (ts, m)
            (_, Just _) -> (ts, m)
            _ -> (st, m)
  in snd . mapAccumL f Nothing

-- | Group measures per system.
mm_measures_per_system :: [Int] -> [Measure] -> [Measure]
mm_measures_per_system n mm =
  let f (m0 : l) = m_annotate_pre system_break m0 : l
      f [] = error "mm_measures_per_system"
  in case Split.splitPlaces n mm of
      g0 : l -> concat (g0 : map f l)
      _ -> mm

-- | Prepend 'system_break' at every nth measure.
mm_measures_per_system_eq :: Int -> [Measure] -> [Measure]
mm_measures_per_system_eq n =
  let f k m =
        if k /= 0 && k `mod` n == 0
          then m_annotate_pre system_break m
          else m
  in zipWith f [0 ..]

-- * Rehearsal marks

default_rehearsal_mark :: Music
default_rehearsal_mark = Command (Rehearsal_Mark Nothing) []
