-- | Paper related functions and constants.
module Music.LilyPond.Light.Paper where

import qualified Music.Theory.Tuple as T {- hmt-base -}

import Music.LilyPond.Light.Model

-- * Paper

-- | Page size, (width,height)
type Size t = (t, t)

-- | Page size in mm.
type Size_Mm = (Double, Double)

-- | Page margins in CSS order (top,right|outer,bottom,left|inner)
type Margins t = (t, t, t, t)

-- | Page margins in mm.
type Margins_Mm = Margins Double

-- | Set margins, ordering as for CSS, ie. clockwise from top.
paper_margins :: Margins Length -> Paper -> Paper
paper_margins (t, r, b, l) p =
  p
    { top_margin = t
    , right_or_outer_margin = r
    , bottom_margin = b
    , left_or_inner_margin = l
    }

-- | Set margins in mm.
paper_margins_mm :: Margins_Mm -> Paper -> Paper
paper_margins_mm = paper_margins . T.t4_map (\x -> Length x Mm)

paper_margins_mm_generic :: Real n => Margins n -> Paper -> Paper
paper_margins_mm_generic = paper_margins . T.t4_map (\x -> Length (realToFrac x) Mm)

-- | Make 'Paper' given size and margins, both in mm.
mk_paper :: Size_Mm -> Margins_Mm -> Paper
mk_paper (w, h) (t, r, b, l) =
  Paper
    { binding_offset = Length 0 Mm
    , bottom_margin = Length b Mm
    , indent = Length 15 Mm
    , left_or_inner_margin = Length l Mm
    , paper_width = Length w Mm
    , paper_height = Length h Mm
    , ragged_right = False
    , ragged_last = False
    , ragged_bottom = False
    , ragged_last_bottom = True
    , right_or_outer_margin = Length r Mm
    , top_margin = Length t Mm
    , two_sided = False
    , print_page_number = True
    , min_systems_per_page = Nothing
    , max_systems_per_page = Nothing
    , systems_per_page = Nothing
    , systems_count = Nothing
    , page_count = Nothing
    , system_separator_markup = Nothing
    , system_spacing_basic_distance = Nothing
    , system_spacing_minimum_distance = Nothing
    }

default_margins :: Margins_Mm
default_margins = (5, 10, 6, 10)

a4_paper :: Margins_Mm -> Paper
a4_paper = mk_paper (210, 297)

b4_paper :: Margins_Mm -> Paper
b4_paper = mk_paper (250, 353)

length_scale :: Double -> Length -> Length
length_scale n (Length x u) = Length (n * x) u

paper_incr_size :: Paper -> Paper
paper_incr_size x =
  let wd = paper_width x
      ht = paper_height x
  in x {paper_width = ht, paper_height = length_scale 2 wd}

paper_decr_size :: Paper -> Paper
paper_decr_size x =
  let wd = paper_width x
      ht = paper_height x
  in x {paper_width = length_scale 0.5 ht, paper_height = wd}

a3_paper :: Margins_Mm -> Paper
a3_paper = paper_incr_size . a4_paper

a2_paper :: Margins_Mm -> Paper
a2_paper = paper_incr_size . a3_paper

b3_paper :: Margins_Mm -> Paper
b3_paper = paper_incr_size . b4_paper

b5_paper :: Margins_Mm -> Paper
b5_paper = paper_decr_size . b4_paper

-- | Swap width and height, margins are unchanged.
landscape :: Paper -> Paper
landscape x =
  let wd = paper_width x
      ht = paper_height x
  in x {paper_width = ht, paper_height = wd}

mk_fragment_paper :: Double -> Double -> Paper
mk_fragment_paper w h =
  Paper
    { binding_offset = Length 0 Mm
    , bottom_margin = Length 0 Mm
    , indent = Length 0 Mm
    , left_or_inner_margin = Length 0 Mm
    , paper_width = Length w Mm
    , paper_height = Length h Mm
    , ragged_right = True
    , ragged_last = True
    , ragged_bottom = True
    , ragged_last_bottom = True
    , right_or_outer_margin = Length 0 Mm
    , top_margin = Length 0 Mm
    , two_sided = False
    , print_page_number = False
    , min_systems_per_page = Nothing
    , max_systems_per_page = Nothing
    , systems_per_page = Nothing
    , systems_count = Nothing
    , page_count = Nothing
    , system_separator_markup = Nothing
    , system_spacing_basic_distance = Nothing
    , system_spacing_minimum_distance = Nothing
    }

-- * Booklet page ordering

{- | Page ordering for booklet of /n/ sheets, ie. n × 4 pages.

>>> booklet_n 3
[[12,1,2,11],[10,3,4,9],[8,5,6,7]]
-}
booklet_n :: Int -> [[Int]]
booklet_n n =
  let k = n * 4
      z = [k, 1, 2, k - 1]
      f l = zipWith (+) l [-2, 2, 2, -2]
  in take n (iterate f z)

{- | Variant of 'booklet_n' that calculates /n/ as k ÷ 4.

>>> booklet_k 36 == booklet_n 9
True
-}
booklet_k :: Int -> [[Int]]
booklet_k k =
  case k `divMod` 4 of
    (n, 0) -> booklet_n n
    _ -> error "booklet_k: ?"
