module Music.LilyPond.Light.Measure where

import Data.List {- base -}

import qualified Music.LilyPond.Light.Constant as C {- hly -}
import Music.LilyPond.Light.Model {- hly -}

type M_Annotation = Music
data Measure = Measure [M_Annotation] [Music] deriving (Eq, Show)

-- | Prepend annotation to existing annotations at measure.
m_annotate_pre :: M_Annotation -> Measure -> Measure
m_annotate_pre z (Measure as xs) = Measure (z : as) xs

-- | Append annotation to existing annotations at measure.
m_annotate :: M_Annotation -> Measure -> Measure
m_annotate z (Measure as xs) = Measure (as ++ [z]) xs

-- | List variant
m_annotate_l :: [M_Annotation] -> Measure -> Measure
m_annotate_l z (Measure as xs) = Measure (as ++ z) xs

-- | Append to end of existing measure.
m_append :: [Music] -> Measure -> Measure
m_append z (Measure as xs) = Measure as (xs ++ z)

m_elements :: Measure -> [Music]
m_elements (Measure as xs) = as ++ xs

-- * Mm = measure sequence

mm_annotate_first_l :: [M_Annotation] -> [Measure] -> [Measure]
mm_annotate_first_l z xs =
  case xs of
    (x : xs') -> m_annotate_l z x : xs'
    [] -> error "mm_annotate_first_l"

mm_annotate_last_l :: [M_Annotation] -> [Measure] -> [Measure]
mm_annotate_last_l z xs =
  case xs of
    [] -> []
    [x] -> [m_annotate_l z x]
    (x : xs') -> x : mm_annotate_last_l z xs'

mm_elements :: [Measure] -> [Music]
mm_elements = concat . intersperse [C.bar_line_check] . map m_elements
