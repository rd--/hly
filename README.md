hly - haskell lilypond
---------------------------------

A lightweight embedding of the [lilypond][ly] typesetting model
in [haskell][hs].

See [hts](?t=hts) for an alternative writing to [MusicXML][music-xml],
and [hmt-texts](?t=hmt-texts) for related work.

[hs]: http://haskell.org/
[ly]: http://lilypond.org/
[music-xml]: http://www.makemusic.com/musicxml/

© [rohan drape][rd], 2010-2025, [gpl]

[rd]: http://rohandrape.net/
[gpl]: http://gnu.org/copyleft/
