- _He will now think he hears her_. (2015) [→](?t=the-center-is-between-us&e=hears/README)
- _Another Mind Is Signaling You_. (2014) [→](?t=another-mind)
- _Autochthons (#1 — #5)_. (2014) [→](?t=sp-id&e=md/autochthons.md)
- _Robert Smithson: Displacements (#1—8, #10—18)_. (2014) [→](?t=sp-id&e=md/displacements.md)
- _Guild∘Psaltery (for AP, after LP)_. (2012) [→](?t=guild-psaltery)
- _and I suffered a complete collapse_. (2011) [→](?t=collapse)
- _Byzantium_. (2010) [→](?t=byzantium)

